#include <stdio.h>
#include <stdlib.h>
#include "funkcje.h"
#include "struktura.h"



int main()
{
    char *filename="P0001_attr.rec.txt";

    FILE* plik;
    plik= fopen(filename, "r");

    if ( plik == NULL)
    {
      printf( "Otwarcie pliku nie powiodlo sie");
      exit(1);
    }

    float *x;
    x = (float*)malloc(50 * sizeof(float));
    float *y;
    y = (float*)malloc(50 * sizeof(float));
    float *RHO;
    RHO = (float*)malloc(50 * sizeof(float));

    int lp[50];
    char dot = ' ';     

    int i=0;
    char *line = NULL;
    size_t len = 0;
    
    int statystyki_istnieja = 0;
    
    getline(&line, &len, plik);
    puts(line); 

    while(i < 50) 
    {
      fscanf(plik, "%d %c %f %f %f", &lp[i], &dot, &x[i], &y[i], &RHO[i]);
          printf("Wczytany wiersz: %f %f %f\n", x[i], y[i], RHO[i]);

      i++;
      
    }

      
    fclose(plik);


    int n = i;
    float sredniaxs = srednia(x,n);
    printf("Srednia dla X z %d liczb wynosi: %f\n", n, sredniaxs);
    
    float sredniays = srednia(y,n);
    printf("Srednia dla Y z %d liczb wynosi: %f\n", n, sredniays);
    
    float sredniarhos = srednia(RHO,n);
    printf("Srednia dla RHO z %d liczb wynosi: %f\n", n, sredniarhos);

    float odchyleniexs = odchylenie(x,n,sredniaxs);
    printf("Odchylenie dla X z %d liczb wynosi: %f\n", n, odchyleniexs);

    float odchylenieys = odchylenie(y,n,sredniays);
    printf("Odchylenie dla Y z %d liczb wynosi: %f\n", n, odchylenieys);

    float odchylenierhos = odchylenie(RHO,n,sredniarhos);
    printf("Odchylenie dla RHO z %d liczb wynosi: %f\n", n, odchylenierhos);

    float medianaxs = mediana(x,n);
    printf("Mediana dla X z %d liczb wynosi: %f\n", n, medianaxs);

    float medianays = mediana(y,n);
    printf("Mediana dla Y z %d liczb wynosi: %f\n", n, medianays);

    float medianarhos = mediana(RHO,n);
    printf("Mediana dla RHO z %d liczb wynosi: %f\n", n, medianarhos);


    struct Statystyki stats;
    stats.dane_x = x[i];
    stats.dane_y = y[i];
    stats.dane_rho = RHO[i];
    stats.srednia_x = sredniaxs;
    stats.srednia_y = sredniays;
    stats.srednia_rho = sredniarhos;
    stats.odchylenie_x = odchyleniexs;
    stats.odchylenie_y = odchylenieys;
    stats.odchylenie_rho = odchylenierhos;
    stats.mediana_x = medianaxs;
    stats.mediana_y = medianays;
    stats.mediana_rho = medianarhos;

    if (statystyki_istnieja == 0) {
      plik = fopen(filename, "a+");
      

      if ( plik == NULL)
      {
        printf( "Otwarcie pliku nie powiodlo sie");
        exit(1);
      }

      fprintf(plik, "\n# Srednia X: %.2f | Srednia Y: %.2f | Srednia RHO: %.2f\n", stats.srednia_x, stats.srednia_y, stats.srednia_rho);
      fprintf(plik, "# Odchylenie X: %.2f | Odchylenie Y: %.2f | Odchylenie RHO: %.2f\n", stats.odchylenie_x, stats.odchylenie_y, stats.odchylenie_rho);
      fprintf(plik, "# Mediana X: %.2f | Mediana Y: %.2f | Mediana RHO: %.2f\n", stats.mediana_x, stats.mediana_y, stats.mediana_rho);

      fclose(plik);
    }

    free(x);
    free(y);
    free(RHO);
    
    return 0;
}