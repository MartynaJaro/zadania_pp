#include <stdlib.h>
#include <stdio.h>
#include "funkcje.h"
#include "struktury.h"
#define N 50

int main()
{    
    char *filename="P0001_attr.rec";

    FILE* plik;
    plik= fopen(filename, "r");

    size_t i, liczba;
    int tab1[50];
    char line[50];
    char linia[100000]="";

    int *rest = (int*) malloc(50 * sizeof(int));    
    float *x = (float*) malloc(50 * sizeof(float));
    float *y = (float*) malloc(50 * sizeof(float));
    float *RHO = (float*) malloc(50 * sizeof(float));
    


    if(plik == NULL)
    
        printf("Cos poszlo nie tak");
    
    else    
    {    
            for( liczba=0; liczba<51; liczba++)        
            {
                if (liczba == 0)
                {
                    fgets(linia, 1000, plik);
                }else{
                    fscanf (plik,"%d %c %f %f %f",&tab1[liczba], &line[liczba], &x[liczba], &y[liczba], &RHO[liczba]);
                }
            }
            for ( i=1; i<liczba; i++ )
            {
                printf("X = %.5f, Y = %.5f, RHO = %.5f\n",x[i], y[i], RHO[i]);
            }
    }


    fclose(plik);


    float srednia_x = srednia(x, N);
    printf("\nSrednia dla X wynosi: %f\n", srednia_x);
    
    float srednia_y = srednia(y, N);
    printf("Srednia dla Y wynosi: %f\n", srednia_y);
    
    float srednia_rho = srednia(RHO, N);
    printf("Srednia dla RHO wynosi: %f\n", srednia_rho);

    float mediana_x = mediana(x, N);
    printf("Mediana dla X wynosi: %f\n", mediana_x);

    float mediana_y = mediana(y, N);
    printf("Mediana dla Y wynosi: %f\n", mediana_y);

    float mediana_rho = mediana(RHO, N);
    printf("Mediana dla RHO wynosi: %f\n", mediana_rho);

    float odchylenie_x = odchylenie(x, srednia_x, N);
    printf("Odchylenie dla X wynosi: %f\n", odchylenie_x);

    float odchylenie_y = odchylenie(y, srednia_y, N);
    printf("Odchylenie dla Y wynosi: %f\n", odchylenie_y);

    float odchylenie_rho = odchylenie(RHO, srednia_rho, N);
    printf("Odchylenie dla RHO wynosi: %f\n", odchylenie_rho);


    struct Statystyki stat;
    stat.srednia_x = srednia_x;
    stat.srednia_y = srednia_y;
    stat.srednia_rho = srednia_rho;
    stat.mediana_x = mediana_x;
    stat.mediana_y = mediana_y;
    stat.mediana_rho = mediana_rho;
    stat.odchylenie_x = odchylenie_x;
    stat.odchylenie_y = odchylenie_y;
    stat.odchylenie_rho = odchylenie_rho;


    plik= fopen(filename, "a+");      // dopisywanie wyników do pliku

    if(plik == NULL)
    
        printf("Cos poszlo nie tak");
    
    else    
    {  
        fprintf(plik, "Srednia X: %f  Srednia Y: %f  Srednia RHO: %f\n", srednia_x, srednia_y, srednia_rho);
        fprintf(plik, "Mediana X: %f  Mediana Y: %f  Mediana RHO: %f\n", mediana_x, mediana_y, mediana_rho);
        fprintf(plik, "Odchylenie X: %f  Odchylenie Y: %f  Odchylenie RHO: %f\n", odchylenie_x, odchylenie_y, odchylenie_rho);
    }


    return 0;
}
