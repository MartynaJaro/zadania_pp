#include <math.h>
#include "funkcje.h"

float odchylenie (float x[], float srednia, float N)
{
    int i=0;
    float odchyl = 0.0;

    for (i=0; i<N; i++)
    {
        odchyl+= pow((x[i] - srednia), 2);
    }
    odchyl= sqrt(odchyl/N);

    return odchyl;
}
