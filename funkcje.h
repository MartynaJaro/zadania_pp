#include <stdio.h>

float srednia (float x[], float N);
float mediana (float x[], int N);
float odchylenie (float x[], float srednia, float N);


float srednia(float x[], float N)
{
    int i = 0;
    float wynik = 0.0;

    for (i=0; i<50; i++) 
    {
        wynik+= x[i];
        wynik/=50;
    }

    return wynik ;
}


float sortowanie(int *x, int N)   

{   int i=0, j=0, a=0;         
    
    while(1)
    {
       j = 0;
       
       for(i=0 ; i<N-1 ; i++)
       {              
            if(x[i]>x[i+1])        
            {
                a = x[i];
                x[i] = x[i+1];
                x[i+1] = a;
                j = 1;
            }
        
        }     
    if(j==0)
    {
        break;
    }
    }
}

float mediana(float x[], int N)
{
    float mediana=0.0;       
    
    if(N%2 == 0)     
    {
        mediana = x[(N-1)/2] + x[N/2];              // mediana dla parzystych
        mediana = mediana/2;
    }
    else
    
        mediana = x[(N+1)/2];                           // mediana dla nieparzystych
    
    return mediana;
}

float odchylenie (float x[], float srednia, float N)
{
    int i=0;
    float odchyl = 0.0;

    for (i=0; i<N; i++)
    {
        odchyl+= pow((x[i] - srednia), 2);
    }
    odchyl= sqrt(odchyl/N);

    return odchyl;
}

