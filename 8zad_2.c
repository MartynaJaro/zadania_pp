#include <stdio.h>
#include <stdlib.h>
#include "funkcje.h"
#include "struktura.h"


int main()
{
    char *filename="P0001_attr.rec.txt";

    FILE* plik;
    plik= fopen(filename, "r");

    if ( plik == NULL)
    {
      printf( "Otwarcie pliku nie powiodlo sie");
      exit(1);
    }

    char x[100];
    char y[100];
    char RHO [100];
    float xs[100];
    float ys[100];
    float RHOs[100];

    int i=0;
    char *line;
    size_t len = 0;
    int read;

    //int statystyki_istnieja = 0;
    
    while ((read = getline(&line, &len, plik)) != -1)
    {
      if (line[0] == 'L' && line[1] == 'P') continue;
      if (line[0] == '#') {
        //statystyki_istnieja = 1;
        continue;
      }

      sscanf(line, "%*s\t%s\t\t%s\t%s", x, y, RHO);
      xs[i]=atof(x);
      ys[i]=atof(y);
      RHOs[i]=atof(RHO);

      printf("Wczytany wiersz: %f %f %f\n", xs[i], ys[i], RHOs[i]);

      i++;
      printf("i = %d\n", i);
    }
    
    fclose(plik);

    int n = i;
    float sredniaxs = srednia(xs,n);
    printf("Srednia dla X z %d liczb wynosi: %f\n", n, sredniaxs);
    
    float sredniays = srednia(ys,n);
    printf("Srednia dla Y z %d liczb wynosi: %f\n", n, sredniays);
    
    float sredniarhos = srednia(RHOs,n);
    printf("Srednia dla RHO z %d liczb wynosi: %f\n", n, sredniarhos);

    float odchyleniexs = odchylenie(xs,n,sredniaxs);
    printf("Odchylenie dla X z %d liczb wynosi: %f\n", n, odchyleniexs);

    float odchylenieys = odchylenie(ys,n,sredniays);
    printf("Odchylenie dla Y z %d liczb wynosi: %f\n", n, odchylenieys);

    float odchylenierhos = odchylenie(RHOs,n,sredniarhos);
    printf("Odchylenie dla RHO z %d liczb wynosi: %f\n", n, odchylenierhos);

    float medianaxs = mediana(xs,n);
    printf("Mediana dla X z %d liczb wynosi: %f\n", n, medianaxs);

    float medianays = mediana(ys,n);
    printf("Mediana dla Y z %d liczb wynosi: %f\n", n, medianays);

    float medianarhos = mediana(RHOs,n);
    printf("Mediana dla RHO z %d liczb wynosi: %f\n", n, medianarhos);


    struct Statystyki stats;
    stats.srednia_x = sredniaxs;
    stats.srednia_y = sredniays;
    stats.srednia_rho = sredniarhos;
    stats.odchylenie_x = odchyleniexs;
    stats.odchylenie_y = odchylenieys;
    stats.odchylenie_rho = odchylenierhos;
    stats.mediana_x = medianaxs;
    stats.mediana_y = medianays;
    stats.mediana_rho = medianarhos;

    //if (statystyki_istnieja == 0) {
      plik = fopen(filename, "a+");
      

      if ( plik == NULL)
      {
        printf( "Otwarcie pliku nie powiodlo sie");
        exit(1);
      }

      fprintf(plik, "\n# Srednia X: %.2f | Srednia Y: %.2f | Srednia RHO: %.2f\n", stats.srednia_x, stats.srednia_y, stats.srednia_rho);
      fprintf(plik, "# Odchylenie X: %.2f | Odchylenie Y: %.2f | Odchylenie RHO: %.2f\n", stats.odchylenie_x, stats.odchylenie_y, stats.odchylenie_rho);
      fprintf(plik, "# Mediana X: %.2f | Mediana Y: %.2f | Mediana RHO: %.2f\n", stats.mediana_x, stats.mediana_y, stats.mediana_rho);

      fclose(plik);
    //}
    
    return 0;
}